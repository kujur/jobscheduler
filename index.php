<?php
if(!(isset($_GET['n']) && $_GET['n']>0)){
	header('location:index.php?n=1');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
   <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/bootbox.min.js"></script>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<style>
body { 
background: url(images/sample-2.jpg) no-repeat center center fixed; 
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;
}</style>
<body>


<div class="navbar-fixed">
<nav class="navbar navbar-inverse" style="height:52px;border-radius:0px">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="#">SAMPLEWEB</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">Page 1</a></li>
        <li><a href="#">Page 2</a></li> 
        <li><a href="#">Page 3</a></li> 
      </ul>
    </div>
  </div>
</nav>
</div>
<style>
.image {position:relative;}
.image .text {position:absolute;top:10px;margin-left:90%;}
.spanhover:hover {cursor:pointer;}
</style>
<script>
var numberofcards = <?php echo $_GET['n']; ?>;
function editContent(id){
	var cardcontent = "cardcontent" + id;
	var insidepcontent = "insidepcontent" + id;
	var cardcontenttextfield = "cardcontenttextfield" + id;
	var text = document.getElementById(insidepcontent).innerHTML;
	if(text=="Double click to edit Text..<br><br>"){
		document.getElementById(cardcontent).innerHTML="<textarea class=\'input-field\' id=\'cardcontenttextfield"+id+"\' onkeydown=\'setContent("+id+")\' style=\'resize:none\'  onblur=\'setContentOnBlur("+id+")\' height=100 width=200></textarea>";
	document.getElementById(cardcontenttextfield).focus();
	}
	else{
	document.getElementById(cardcontent).innerHTML="<textarea class=\'input-field\' id=\'cardcontenttextfield"+id+"\' onkeydown=\'setContent("+id+")\' style=\'resize:none\' height=100 width=200>"+text+"</textarea>";
	document.getElementById(cardcontenttextfield).focus();
	}
}
function setContent(id){
	var cardcontent = "cardcontent" + id;
	var cardcontenttextfield = "cardcontenttextfield"+id;
	var text = document.getElementById(cardcontenttextfield).value;
	 if(event.keyCode != 13) {return false;}
		if(text==""){return false;}
		document.getElementById(cardcontent).innerHTML = '<p ondblclick="editContent('+id+')"  id="insidepcontent'+id+'" style="word-wrap:break-word">'+text+'</p>';
}
function setContentOnBlur(id){
	var cardcontent = "cardcontent" + id;
	var cardcontenttextfield = "cardcontenttextfield"+id;
	var text = document.getElementById(cardcontenttextfield).value;
		if(text==""){document.getElementById(cardcontent).innerHTML = '<p ondblclick="editContent('+id+')"  id="insidepcontent'+id+'" style="word-wrap:break-word">Double click to edit Text..<br><br></p>';}
		document.getElementById(cardcontent).innerHTML = '<p ondblclick="editContent('+id+')"  id="insidepcontent'+id+'" style="word-wrap:break-word">'+text+'</p>';
}
function editTitle(id){
	var cardtitle = "cardtitle" + id;
	var cardtitletextfield = "cardtitletextfield" + id;
	document.getElementById(cardtitle).innerHTML="<span class=\'card-title\'><input type=\'text\' class=\'input-field\' style=\'margin-bottom:0px\' id=\'cardtitletextfield"+id+"\' onkeydown=\'setTitle("+id+")\' onblur=\'setTitleOnBlur("+id+")\'></span>";
	document.getElementById(cardtitletextfield).focus();
}
function setTitle(id){
	var cardtitle = "cardtitle" + id;
	var cardtitletextfield = "cardtitletextfield"+id;
	var text = document.getElementById(cardtitletextfield).value;
	 if(event.keyCode != 13) {return false;}
		if(text==""){return false;}
		document.getElementById(cardtitle).innerHTML = '<span class="card-title" ondblclick="document.getElementById(\'cardtitle'+id+'\').innerHTML=\'<span class=card-title><input type=text class=input-field id=cardtitletextfield'+id+' onkeydown=setTitle('+id+') onblur=setTitleOnBlur('+id+') style=margin-bottom:0px></span>\';">'+text+'</span>';
}
function setTitleOnBlur(id){
	var cardtitle = "cardtitle" + id;
	var cardtitletextfield = "cardtitletextfield"+id;
	var text = document.getElementById(cardtitletextfield).value;
		if(text==""){return false;}
		document.getElementById(cardtitle).innerHTML = '<span class="card-title" ondblclick="document.getElementById(\'cardtitle'+id+'\').innerHTML=\'<span class=card-title><input type=text class=input-field id=cardtitletextfield'+id+' style=margin-bottom:0px onkeydown=setTitle('+id+') onblur=setTitle('+id+')></span>\';">'+text+'</span>';
}
function confirmDel(id){
	cardid = "#card"+id;
	$(cardid).fadeOut("slow", function() {
    	$(cardid).empty();
	});
}
function confirmAdd(id){
	var addelem = document.getElementById("addbuttondiv");
	addelem.innerHTML = "";
	
	var div = '<div class=\"card z-depth-5\"><div class=\"card-image\"><div class=\"image\"><img src=\"images/sample-1.jpg\" /><div class=\"text\"><span onclick=\"confirmDel('+id+')\" href=\"#\" style=\"color:white;font-size:12px;\" class=\"glyphicon glyphicon-remove-circle spanhover\"></span></div><div id=\"cardtitle'+id+'\"><span class=\"card-title\" ondblclick=\"editTitle('+id+')\" style="font-size:20px;">New Card</span></div></div></div><div class=\"card-reveal\"><span class=\"card-title\">DETAILS<i class="material-icons right">close</i></span><div id=\"cardcontent'+id+'\" style=\"height:90px;margin-top:10px;overflow-y:auto;font-size:12px\"><p ondblclick=\"editContent('+id+')\" id=\"insidepcontent'+id+'\"  style=\"word-wrap:break-word\">Double click to edit Text..<br><br></p></div></div><div class=\"card-action\"><a href=\"#\" class=\"activator\">SHOW DETAILS</a></div></div>';
	var addelem = document.getElementById("addbuttondiv");
	addelem.innerHTML = div;
	$("#addbuttondiv").fadeIn(3000);
	addelem.setAttribute("id","card"+id);
	addelem.setAttribute("style","");
	setTimeout(function(){}, 1000);
	var updatebutton = document.getElementById("rowC");
	updatebutton.innerHTML += '<div class="col-sm-3" id="addbuttondiv" style="text-align:center;padding-top:7%"> </div>';
	var fixedupdatebutton = document.getElementById("fixedaddbutton");
	fixedupdatebutton.innerHTML = '<a class="btn-floating btn-large blue" onclick="confirmAdd('+(id+1)+')"><i class=\"large material-icons\">add</i></a>';
	/*cardid = "#card"+id;
	$(cardid).fadeOut("slow", function() {
    	$(cardid).removeClass('hidden');
	});*/
}
</script>
<div class="container">
  <div class="row" id="rowC">
    <?php
	  function echo_card($id){
	  	echo '<div class="col-sm-3" id="card'.$id.'">
		     <div class="card z-depth-5">
		            <div class="card-image">
		              <div class="image">
		  				<img alt="" src="images/sample-1.jpg" />
		  				<div class="text">
		    				<span onclick="confirmDel('.$id.')" href="#" style="color:white;font-size:12px;" class="glyphicon glyphicon-remove-circle spanhover"></span>
		  				</div><div id="cardtitle'.$id.'">
		  				<span class="card-title" ondblclick="editTitle('.$id.')" style="font-size:20px;">Card no. '.$id.'</span></div>
					  </div>
		            </div>

		            <div class="card-reveal">
		            	 <span class="card-title">DETAILS<i class="material-icons right">close</i></span>
		              <div id="cardcontent'.$id.'" style="height:90px;overflow-y:auto;font-size:12px;margin-top:10px">
		  				<p ondblclick="editContent('.$id.')" id="insidepcontent'.$id.'"  style="word-wrap:break-word">Double click to edit Text..<br><br></p>
					  </div>
		            </div>
		            <div class="card-action">
		              <a href="#" class="activator">SHOW DETAILS</a>
		            </div>
		          </div>
		    </div>';
	  }
	  $n=$_GET['n'];
	  $row=(int)($n/4);
	  $remainder = $n%4;
	for($j=0;$j<$row;$j++){
		  for($i=1;$i<=4;$i++){
		  	$id = $j*4+$i;
		    echo_card($id);
		}
	}
	for($i=1;$i<=$remainder;$i++){
	    $id = $i+$row*4;
	    echo_card($id);
	}
	 echo '<div class="col-sm-3" id="addbuttondiv" style="text-align:center;padding-top:7%"> </div>';/*<br><br> <button class="btn waves-effect waves-light blue" type="submit" name="action">Submit
    <i class="material-icons right">send</i>
  </button></div>';*/
	/*for(;$i<4;$i++){
	   echo '<div class="col-sm-3"></div>';
	}*/
?>
  </div>
  <div class="row">
  	<div class="col-sm-4"></div>
  	<div class="col-sm-4">
  		
  	</div>
  	<div class="col-sm-4"></div>
  	</div>
</div>
<div class="fixed-action-btn" id="fixedaddbutton">
    <a class="btn-floating btn-large blue" onclick="confirmAdd('<?php echo ($_GET['n']+1); ?>')">
      <i class="large material-icons">add</i>
    </a>
  </div>
</body>
</html>

